[[block]] struct Array {

    numbers : array<f32>;
};

[[block]] struct UBO {
  channelIdxs: array<i32, 2>;
  outputSizes: array<i32, 2>;
  inputSizes: array<i32, 3>;
  kernSizes: array<i32, 4>;
};

[[group(0), binding(0)]] var<storage, read> inputImage : Array;
[[group(0), binding(1)]] var<storage, read> inputKernel : Array;
[[group(0), binding(2)]] var<storage, read> inputBias : Array;
[[group(0), binding(3)]] var<storage, read_write> resultImage : Array;
[[group(0), binding(4)]] var<storage, read> ufs : UBO;

[[stage(compute), workgroup_size(1, 1)]]
fn main([[builtin(global_invocation_id)]] global_id : vec3<u32>) {
    // Guard against out-of-bounds work group sizes.




    resultImage.numbers[0] = 7.0;
}
